import {Component} from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  subscriptions = [
    { key: 'basic', value: 'Basic' },
    { key: 'pro', value: 'Pro' },
    { key: 'advanced', value: 'Advanced' }
  ];

  subscription = 'advanced';

  results: {email: string, subscription: string, password: string};

  onSubmit(f: NgForm) {
    console.log(f.value);
    this.results = {
      email: f.value.email,
      subscription: f.value.subscription,
      password: f.value.password
    }
  }
}
